
package main

import (
      "fmt"
      "net/http"
      "os"
      "log"
      "sync"
      "time"
      "encoding/json"
      "github.com/robbert229/jwt"
)


var prodMutex = new(sync.Mutex)
var shopcartMutex = new(sync.Mutex)


func handleAuthentication(w http.ResponseWriter, r *http.Request) {
   prodMutex.Lock()
   defer prodMutex.Unlock()

   switch r.Method {
     case "POST":
        secret := "ThisIsMySuperSecret"
        log.Println("Serv IN GET")
	algorithm := jwt.HmacSha256(secret)

	claims := jwt.NewClaim()
	claims.Set("Role", "ShopCustomer")
	claims.SetTime("exp", time.Now().Add(time.Minute))

	token, err := algorithm.Encode(claims)
	if err != nil {
	  panic(err)
	}

	if algorithm.Validate(token) != nil {
	  panic(err)
	}

  tk, err := json.Marshal(token)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }
  w.Header().Set("Access-Control-Allow-Origin", "*")
  w.Header().Set("Content-Type", "application/jwt")
	w.Header().Set("Authorization", fmt.Sprintf("Bearer %s", token))

	w.WriteHeader(http.StatusOK)
        w.Write(tk)

     default:
       http.Error(w, fmt.Sprintf("unsupported method: %s", r.Method), http.StatusMethodNotAllowed)
   }

}



func main() {

    port := os.Getenv("AUTHSRVPORT")
    if port == "" {
            port = "4400"
    }


    http.HandleFunc("/auth", handleAuthentication)

    log.Println("Server started: http://localhost:" + port)
    log.Fatal(http.ListenAndServe(":"+port, nil))

}
