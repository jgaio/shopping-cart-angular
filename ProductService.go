
package main

import (
      "fmt"
      "net/http"
      "os"
      "log"
      "sync"
      "time"
      "gopkg.in/mgo.v2"
      "gopkg.in/mgo.v2/bson"
      "encoding/json"
      "github.com/robbert229/jwt"
)

type Product struct {
  _Id string
  Name string
  Image string
  Price float64
  ProdId int
}

type ShoppingCart struct {
  Token string
  Amount float64
  Items []Item
}

type Item struct {
  _Id string
  Name string
  Price float64
  Amount float64
  Qtd string
  ProdId int
}

var prodMutex = new(sync.Mutex)
var shopcartMutex = new(sync.Mutex)


func handleShoppingCart(w http.ResponseWriter, r *http.Request) {
   shopcartMutex.Lock()
   defer shopcartMutex.Unlock()

   switch r.Method {

     case "POST":

       item := ShoppingCart{}

       json.NewDecoder(r.Body).Decode(&item)

       session, err := mgo.Dial("localhost")
       if err != nil {
          panic(err)
       }
       defer session.Close()

       // Optional. Switch the session to a monotonic behavior.
       session.SetMode(mgo.Monotonic, true)

       c := session.DB("shopping").C("teste")
       if err != nil {
          panic(err)
       }

       c.Insert(item)
       w.Header().Set("Access-Control-Allow-Origin", "*")
       w.Header().Set("Content-Type","application/json")
       w.WriteHeader(201)

     case "PUT":

       token := r.URL.Query().Get("token")

       log.Println(token)

       item := Item{}

       json.NewDecoder(r.Body).Decode(&item)
       log.Println(item.Name)

       session, err := mgo.Dial("localhost")
       if err != nil {
          panic(err)
       }
       defer session.Close()

       // Optional. Switch the session to a monotonic behavior.
       session.SetMode(mgo.Monotonic, true)

       c := session.DB("shopping").C("teste")
       if err != nil {
          panic(err)
       }

       qs := bson.M{"token": token}
       // Update
       change := bson.M{"$push": bson.M{"items": item }}
       err = c.Update(qs, change)
       if err != nil {
	 panic(err)
       }

       //var result ShoppingCart
       result := ShoppingCart{}
       err = c.Find(bson.M{"token": token}).One(&result)
       newamount := item.Amount + result.Amount
       change = bson.M{"$set": bson.M{"amount": newamount }}
       err = c.Update(qs, change)
       if err != nil {
	 panic(err)
       }

       w.Header().Set("Access-Control-Allow-Origin", "*")
       w.Header().Set("Content-Type","application/json")
       w.WriteHeader(201)

     case "GET":

       token := r.URL.Query().Get("token")
       session, err := mgo.Dial("localhost")
        if err != nil {
           panic(err)
        }
        defer session.Close()

        // Optional. Switch the session to a monotonic behavior.
        session.SetMode(mgo.Monotonic, true)

        c := session.DB("shopping").C("teste")
        if err != nil {
           log.Fatal(err)
        }

        var result []ShoppingCart

        err = c.Find(bson.M{"token": token}).All(&result)
        if err != nil {
           log.Fatal(err)
        }

        js, err := json.Marshal(result)
        if err != nil {
          http.Error(w, err.Error(), http.StatusInternalServerError)
          return
        }

        fmt.Println("All results: ", result)
        w.Header().Set("Access-Control-Allow-Origin", "*")
        w.Header().Set("Content-Type", "application/json")
        w.Write(js)

     default:
       http.Error(w, fmt.Sprintf("unsupported method: %s", r.Method), http.StatusMethodNotAllowed)
   }

}

func handleAuthentication(w http.ResponseWriter, r *http.Request) {
   prodMutex.Lock()
   defer prodMutex.Unlock()

   switch r.Method {
     case "GET":
        secret := "ThisIsMySuperSecret"
	algorithm := jwt.HmacSha256(secret)

	claims := jwt.NewClaim()
	claims.Set("Role", "ShopCustomer")
	claims.SetTime("exp", time.Now().Add(time.Minute))

	token, err := algorithm.Encode(claims)
	if err != nil {
	  panic(err)
	}

	if algorithm.Validate(token) != nil {
	  panic(err)
	}

  tk, err := json.Marshal(token)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }
  w.Header().Set("Access-Control-Allow-Origin", "*")
  w.Header().Set("Content-Type", "application/jwt")
	w.Header().Set("Authorization", fmt.Sprintf("Bearer %s", token))

	w.WriteHeader(http.StatusOK)
        w.Write(tk)

     default:
       http.Error(w, fmt.Sprintf("unsupported method: %s", r.Method), http.StatusMethodNotAllowed)
   }

}


func handleDeleteShopItem(w http.ResponseWriter, r *http.Request) {
   prodMutex.Lock()
   defer prodMutex.Unlock()

   switch r.Method {
     case "POST":

       token := r.URL.Query().Get("token")
       item := Item{}
       json.NewDecoder(r.Body).Decode(&item)

       fmt.Println("item deletet: ", item)
       session, err := mgo.Dial("localhost")
       if err != nil {
         panic(err)
       }
       defer session.Close()

       // Optional. Switch the session to a monotonic behavior.
       session.SetMode(mgo.Monotonic, true)

       c := session.DB("shopping").C("teste")
       if err != nil {
         panic(err)
       }

       qs := bson.M{"token": token}
       // Update
       change := bson.M{"$pull": bson.M{"items": item }}
       err = c.Update(qs, change)
       if err != nil {
	 panic(err)
       }

       result := ShoppingCart{}

       err = c.Find(bson.M{"token": token}).One(&result)
       //amount update
       newamount := result.Amount - item.Amount

       change = bson.M{"$set": bson.M{"amount": newamount }}
       err = c.Update(qs, change)
       if err != nil {
	 panic(err)
       }
       w.Header().Set("Access-Control-Allow-Origin", "*")
       w.Header().Set("Content-Type", "application/json")
       w.WriteHeader(http.StatusOK)

     default:
       http.Error(w, fmt.Sprintf("unsupported method: %s", r.Method), http.StatusMethodNotAllowed)
   }
}


func handleProduct(w http.ResponseWriter, r *http.Request) {
   prodMutex.Lock()
   defer prodMutex.Unlock()

   switch r.Method {
     case "GET":
       session, err := mgo.Dial("localhost")
        if err != nil {
           panic(err)
        }
        defer session.Close()

        // Optional. Switch the session to a monotonic behavior.
        session.SetMode(mgo.Monotonic, true)

        c := session.DB("product").C("products")
        if err != nil {
           panic(err)
        }

        var result []Product
        err = c.Find(nil).All(&result)
        if err != nil {
           panic(err)
        }

        js, err := json.Marshal(result)
        if err != nil {
          http.Error(w, err.Error(), http.StatusInternalServerError)
          return
        }

        fmt.Println("All results: ", result)
        w.Header().Set("Access-Control-Allow-Origin", "*")
        w.Header().Set("Content-Type", "application/json")
        w.Write(js)

     default:
       http.Error(w, fmt.Sprintf("unsupported method: %s", r.Method), http.StatusMethodNotAllowed)
   }
}

func main() {

    port := os.Getenv("PRODUCTSRVPORT")
    if port == "" {
            port = "4400"
    }


    http.HandleFunc("/products", handleProduct)
    http.HandleFunc("/auth", handleAuthentication)
    http.HandleFunc("/shoppingcart", handleShoppingCart)
    http.HandleFunc("/shoppingcart/items", handleDeleteShopItem)
    http.Handle("/", http.FileServer(http.Dir("./public")))
    log.Println("Server started: http://localhost:" + port)
    log.Fatal(http.ListenAndServe(":"+port, nil))

}
