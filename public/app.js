var app = angular.module('ProductApp', ['ngCookies']);


app.controller('ProductsCtrl', ['$scope','$http', '$cookies', function($scope, $http, $cookies) {

  $http.get("/products")
    .then(function(response) {
	  $cookies.remove('token');
      $scope.data = response.data;
    });

  var token=$cookies.get('token');
  if (token != null) {    
    $http.get("/shoppingcart")
     .then(function(response) {
       $scope.datacart = response.data;
     });
  }
 

 
  $scope.deleteItem = function(item) {
    $http.post("/shoppingcart/items", item, {params:{"token": $cookies.token}})
      .then(function(response) {
        $scope.message = response.data;
        $http.get("/shoppingcart", {params:{"token": $cookies.token}})
         .then(function(response) {
           $scope.datacart = response.data[0];
         });

      });
  };

  $scope.postItem = function(prod) {
    var dataobj;
    if($cookies.token == null) {
      //create shoppingcart
      alert(prod._Id);
      $http.get("/auth")
        .then(function(resp) {
           $cookies.token = resp.data;
           //set cookie
		   $cookies.put('token', resp.data);
           prod.Amount = prod.Qtd * prod.Price;
           dataobj = {"token": $cookies.token, "amount": prod.Amount ,"items":[prod]}
	       var res = $http.post("/shoppingcart", dataobj)
           res.success(function(data, status, headers, config) {
             $scope.message = data;

             $http.get("/shoppingcart", {params:{"token": $cookies.token}})
              .then(function(response) {
                $scope.datacart = response.data[0];
              });

           });
           res.error(function(data, status, headers, config) {
             alert("failure message: " + JSON.stringify({data: data}))
           });
       });
    }else{
      //update shoppingcart
      prod.Amount = prod.Qtd * prod.Price;
      var res = $http.put("/shoppingcart", prod, {params:{ "token": $cookies.token}})
      res.success(function(data, status, headers, config) {
        $scope.message = data;

        $http.get("/shoppingcart", {params:{"token": $cookies.token}})
         .then(function(response) {
         	alert("get"+ prod._Id);
           $scope.datacart = response.data[0];
         });

      });
    }
  };

}]);
