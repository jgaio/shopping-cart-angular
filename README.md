# Shopping Cart Teste

Has been developed an application to present product's catalog and shopping cart, enabling consumers to add and remove products from basket and checking items, prices, quantities, and amounts.
The client-server architecture has been done with REST API services

Technologies:
Front-end developed in AngularJS and back-end developed in GO (golang) and MongoDB 


## Config and Run local server

1. Make sure you have [Go] installed.
2. Make sure you have [Mgo Driver] installed.
3. Make sure you have [MongoDB] installed.
4. Make sure the `Mgo` is in your `$GOPATH`
5. Import Mongo Collections to properly Database 

### Uses (app root folder) 

1. `go run ProductServer.go`
3. visit `http://localhost:4400`

### ZIP File
Files included in zip, index.html app.js (public folder), ProductService.go (and its bin), products.json shoppingcarts.json (mongoscript folder), image folder containing products images and also this README.md file.
